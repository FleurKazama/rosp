document.addEventListener('DOMContentLoaded', function () {
    savingToStorageInit();
    alertDisplayingInit();
    loadingFromStorageInit();
    sendingFormInit();
    aliasValidationInit();
    phoneMaskInit();
});

function savingToStorageInit() {

    var storableField = document.getElementsByClassName('storable-field');

    for (var i = 0; i < storableField.length; i++) {

        storableField[i].addEventListener('change', function (e) {

            var value = e.target.type === 'checkbox' ? +e.target.checked : e.target.value;

            window.localStorage.setItem(e.target.name, value);

            window.localStorage.setItem('unsavedData', true);

        });

    }
}

function loadingFromStorageInit() {

    var storableField = document.getElementsByClassName('storable-field');

    for (var i = 0; i < storableField.length; i++) {

        if (storableField[i].type === 'checkbox') {

            var value = !!+window.localStorage.getItem(storableField[i].name);

            storableField[i].checked = value;

        } else {

            storableField[i].value = window.localStorage.getItem(storableField[i].name);

        }

    }

}

function alertDisplayingInit() {

    if (window.localStorage.getItem('unsavedData')) {

        document.querySelector('.alert-block').classList.add('alert-block--shown');

    }

}

function sendingFormInit() {

    document.querySelector('.button-send').addEventListener('click', function () {

        window.localStorage.removeItem('unsavedData');

        document.querySelector('.alert-block').classList.remove('alert-block--shown');

    });

}

function aliasValidationInit() {

    document.querySelector('.alias').addEventListener('input', function (e) {

        validateAlias(e.target);

    });

    validateAlias(document.querySelector('.alias'));

}

function phoneMaskInit() {
    document.querySelector('.phone').addEventListener('input', phoneMask);
}

function validateAlias(el) {

    var isInvalid = /[а-я\s]+/ig.test(el.value);

    var classList = document.querySelector('.info__alias-label').classList;

    isInvalid ? classList.add('info__label--has-error') : classList.remove('info__label--has-error');

}

function phoneMask(e) {

    // https://stackoverflow.com/a/29335409

    var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/);

    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '') + (x[4] ? '-' + x[4] : '');

}